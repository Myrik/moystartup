from datetime import datetime, timedelta
from unittest import TestCase
from unittest.mock import patch
from uuid import uuid4

import requests_mock
from requests import Session

import create_invoice as ci
from create_invoice import create_invoice
from schemes import Currency


class TestInvoice(TestCase):
    maxDiff = None

    def test_1(self):
        contractor_id = uuid4()
        owner_id = uuid4()
        service_id = uuid4()
        currency_id = uuid4()
        bank_account_id = uuid4()
        vat_rate_id = uuid4()

        class Settings:
            PER_HOUR = 10
            USD_SALARY = 1000
            USD_PREMIUM = 30
            CONTRACTOR_NAME = "STARTUPMILL OÜ"

        with requests_mock.Mocker() as m:
            m.get(
                "https://wise.com/rates/history+live",
                json=[
                    {
                        "source": "USD",
                        "target": "PLN",
                        "value": 3.84205,
                        "time": 1631196000000,
                    }
                ],
            )

            page = f"""<div>
            <input name='sales_invoice_proforma_form[_token]' value='_token'>
            <input id='ownerId' value='{owner_id}'>
            <select name='sales_invoice_billing_form[currency]'>
                <option value='{currency_id}'>USD</option>
            </select>
            <select name='sales_invoice_prepayment_form[bankAccount]'>
                <option value='{bank_account_id}'>USD</option>
            </select>
            </div>"""

            m.get("https://crm.twojstartup.pl/sales-invoices/add", text=page)

            today = datetime.today().date()
            m.get(
                f"https://crm.twojstartup.pl/api/sales-invoices/5/nextNumber/{today}/owner",
                json=123,
            )
            m.get(
                f"https://crm.twojstartup.pl/api/warehouse/owner/{owner_id}/services?name=IT+services",
                json=[
                    {
                        "id": str(service_id),
                        "name": "IT services",
                    }
                ],
            )
            m.get(
                f"https://crm.twojstartup.pl/api/warehouse/services/{service_id}",
                json={
                    "id": str(service_id),
                    "name": "IT services",
                    "default_price": 0,
                    "accepted": None,
                    "created_at": "2020-07-20 14:13:44",
                    "updated_at": "2020-07-20 14:13:44",
                    "deleted_at": None,
                    "split_payment": False,
                    "default_unit": "service",
                    "vat_rate": str(vat_rate_id),
                    "global": True,
                    "gtu": None,
                },
            )
            m.get(
                f"https://crm.twojstartup.pl/api/contractors/offeror/{owner_id}",
                json=[{"name": Settings.CONTRACTOR_NAME, "id": str(contractor_id)}],
            )

            with patch.object(
                ci, "CURRENCY_NAMES", {Currency.USD: "USD"}
            ), patch.object(ci, "settings", Settings()):
                with Session() as s:
                    invoice = create_invoice(s, Currency.USD, 0)

            self.assertDictEqual(
                invoice.output(),
                {
                    "salesInvoiceType": 5,
                    "sales_invoice_proforma_form[_token]": "_token",
                    "sales_invoice_proforma_form[bankAccount]": str(bank_account_id),
                    "sales_invoice_proforma_form[comment]": "",
                    "sales_invoice_proforma_form[contractorName]": Settings.CONTRACTOR_NAME,
                    "sales_invoice_proforma_form[contractor]": str(contractor_id),
                    "sales_invoice_proforma_form[currency]": str(currency_id),
                    "sales_invoice_proforma_form[internalComment]": "",
                    "sales_invoice_proforma_form[mailOnStatusChange]": "1",
                    "sales_invoice_proforma_form[number]": "123",
                    "sales_invoice_proforma_form[payment][exchange]": "3.84205",
                    "sales_invoice_proforma_form[payment][expiresAt]": str(
                        datetime.today().date() + timedelta(days=7),
                    ),
                    "sales_invoice_proforma_form[payment][paymentType]": "transfer",
                    "sales_invoice_proforma_form[payment][status]": "unpaid",
                    "sales_invoice_proforma_form[payment][transaction]": "foreign",
                    "sales_invoice_proforma_form[sendToContractorOnAccept]": "1",
                    "sales_invoice_proforma_form[services][0][amount]": "1",
                    "sales_invoice_proforma_form[services][0][gross]": "4348",
                    "sales_invoice_proforma_form[services][0][gtu]": "GTU_12",
                    "sales_invoice_proforma_form[services][0][serviceName]": "IT services",
                    "sales_invoice_proforma_form[services][0][service]": str(
                        service_id
                    ),
                    "sales_invoice_proforma_form[services][0][splitPayment]": "0",
                    "sales_invoice_proforma_form[services][0][unitPrice]": "4348",
                    "sales_invoice_proforma_form[services][0][unit]": "service",
                    "sales_invoice_proforma_form[services][0][vatRate]": str(
                        vat_rate_id
                    ),
                    "sales_invoice_proforma_form[soldAt]": str(datetime.today().date()),
                    "sales_invoice_proforma_form[submit]": "",
                },
            )
