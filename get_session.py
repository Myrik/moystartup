from lxml import html
from requests import Session

from schemes import SITE


def get_session(username: str, password: str) -> Session:
    session = Session()
    page = session.get(f"{SITE}/login")
    tree = html.fromstring(page.content)
    csrf_token = tree.xpath('//input[@name="_csrf_token"]/@value')[0]
    result = session.post(
        f"{SITE}/login",
        data={"_username": username, "_password": password, "_csrf_token": csrf_token},
    )
    assert "/login" not in result.url
    return session
