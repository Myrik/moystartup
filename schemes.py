import re
from collections import defaultdict
from datetime import date
from enum import Enum
from typing import Dict, Optional
from uuid import UUID

from pydantic import BaseModel, Field

from config import settings

SITE = "https://crm.twojstartup.pl"
API = f"{SITE}/api"


class Currency(str, Enum):
    PLN = "PLN"
    USD = "USD"
    EUR = "EUR"

    def __str__(self):
        return self.value


class Service(BaseModel):
    id: UUID
    name: str
    default_price: int
    accepted: Optional[bool]
    created_at: str  # "2020-07-20 14:13:44"
    updated_at: Optional[str]  # "2020-07-20 14:13:44"
    deleted_at: Optional[str]
    split_payment: bool
    default_unit: str
    vat_rate: UUID
    is_global: bool = Field(alias="global")
    gtu: None


class PaymentStatus(str, Enum):
    PAID = "paid"
    UNPAID = "unpaid"

    def __str__(self):
        return self.value


class PaymentType(str, Enum):
    TRANSFER = "transfer"
    CASH = "cash"

    def __str__(self):
        return self.value


class TransactionType(str, Enum):
    NATIONAL = "national"
    FOREIGN = "foreign"

    def __str__(self):
        return self.value


class PaymentField(BaseModel):
    transaction: TransactionType
    payment_type: PaymentType = Field(alias="paymentType")
    status: PaymentStatus
    expires_at: date = Field(alias="expiresAt")
    exchange: str


class ServiceField(BaseModel):
    service_name: str = Field(alias="serviceName")
    service: UUID
    split_payment: bool = Field(alias="splitPayment")
    amount: int
    unit: str = "service"
    unit_price: int = Field(alias="unitPrice")
    vat_rate: UUID = Field(alias="vatRate")
    gtu: str

    def dict(self, *args, **kwargs):
        data = super().dict(*args, **kwargs)
        data["gross"] = self.gross
        return data

    @property
    def gross(self):
        return self.amount * self.unit_price


class SalesInvoiceProformaForm(BaseModel):
    number: str
    sold_at: date = Field(alias="soldAt")
    mail_on_status_change: bool = Field(alias="mailOnStatusChange")
    send_to_contractor_on_accept: bool = Field(alias="sendToContractorOnAccept")
    contractor_name: str = Field(alias="contractorName")
    contractor: UUID
    payment: PaymentField
    currency: UUID
    bank_account: UUID = Field(alias="bankAccount")
    comment: str = ""
    internal_comment: str = Field(default="", alias="internalComment")
    services: Dict[int, ServiceField]

    token: str = Field(alias="_token")
    submit = ""

    @staticmethod
    def sales_invoice_type():
        return 5

    def output(self):
        data = {}
        for k, v in self.dict(by_alias=True).items():
            if isinstance(v, PaymentField):
                v = v.dict()
            if isinstance(v, dict):
                for vk, vv in v.items():
                    if isinstance(vv, ServiceField):
                        vv = vv.dict()

                    if isinstance(vv, dict):
                        for vvk, vvv in vv.items():
                            data[f"[{k}][{vk}][{vvk}]"] = vvv
                    else:
                        data[f"[{k}][{vk}]"] = vv
            else:
                data[f"[{k}]"] = v

        data = {
            "sales_invoice_proforma_form" + k: self.prep_field(v)
            for k, v in data.items()
        }
        return {**data, "salesInvoiceType": self.sales_invoice_type()}

    @staticmethod
    def prep_field(v):
        if isinstance(v, bool):
            return str(int(v))
        else:
            return str(v)

    @classmethod
    def parse(cls, data: dict):
        sub = defaultdict(dict)
        for k, v in data.items():
            k = k.replace("sales_invoice_proforma_form", "")
            keys = re.findall(r"\[(\w+)\]", k)
            if len(keys) == 3:
                if keys[0] not in sub:
                    sub[keys[0]] = {}
                if keys[1] not in sub[keys[0]]:
                    sub[keys[0]][keys[1]] = {}
                sub[keys[0]][keys[1]][keys[2]] = v
            if len(keys) == 2:
                if keys[0] not in sub:
                    sub[keys[0]] = {}
                sub[keys[0]][keys[1]] = v
            elif len(keys) == 1:
                sub[keys[0]] = v
        return cls(**sub)


BANK_ACCOUNTS = {
    Currency.PLN: settings.PLN_ACCOUNT or None,
    Currency.EUR: settings.EUR_ACCOUNT or None,
    Currency.USD: settings.USD_ACCOUNT or None,
}

CURRENCY_NAMES = {
    Currency.PLN: "Polski złoty",
    Currency.EUR: "Euro",
    Currency.USD: "Dolar amerykański",
}
