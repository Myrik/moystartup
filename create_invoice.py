from datetime import datetime, timedelta
from uuid import UUID

from lxml import html
from requests import Session

from config import settings
from get_contractor_id import get_contractor_id
from get_rate import get_rate
from get_service import get_service
from schemes import (
    API,
    BANK_ACCOUNTS,
    CURRENCY_NAMES,
    Currency,
    PaymentField,
    PaymentStatus,
    PaymentType,
    SITE,
    SalesInvoiceProformaForm,
    ServiceField,
    TransactionType,
)


def create_invoice(
    s: Session, currency: Currency, overtime: int
) -> SalesInvoiceProformaForm:
    # overtime = int(input("Input owertime hours [0]: ") or 0)
    currency_name = CURRENCY_NAMES[currency]
    currency_rate = get_rate(Currency.USD, currency)
    print(f"Exchange {Currency.USD!s}-{currency!s}: {currency_rate}")
    usd_salary = (
        (
            float(settings.PER_HOUR * 2 * overtime)
            + float(settings.USD_SALARY + settings.USD_PREMIUM)
        )
        / 91
        * 100
    )
    salary = int(usd_salary * currency_rate)

    bank_account = BANK_ACCOUNTS[currency]
    if not bank_account:
        raise Exception("No such bank account")

    today = datetime.today().date()

    page = s.get(f"{SITE}/sales-invoices/add")

    tree = html.fromstring(page.content)
    token = tree.xpath('//input[@name="sales_invoice_proforma_form[_token]"]/@value')[0]
    owner_id = UUID(tree.xpath('//input[@id="ownerId"]/@value')[0])
    bank_accounts = {
        i.text: UUID(i.attrib["value"])
        for i in tree.xpath(
            '//select[@name="sales_invoice_prepayment_form[bankAccount]"]/option'
        )
    }
    currencies = {
        i.text: UUID(i.attrib["value"])
        for i in tree.xpath(
            '//select[@name="sales_invoice_billing_form[currency]"]/option'
        )
    }

    number = s.get(
        f"{API}/sales-invoices/{SalesInvoiceProformaForm.sales_invoice_type()}/nextNumber/{today}/owner"
    ).json()

    service = get_service(s, owner_id)
    contractor = get_contractor_id(s, owner_id, settings.CONTRACTOR_NAME)

    services = {
        0: ServiceField(
            serviceName=service.name,
            service=service.id,
            splitPayment=service.split_payment,
            amount=1,
            unit=service.default_unit,
            unitPrice=int(salary),
            vatRate=service.vat_rate,
            gtu="GTU_12",
        )
    }

    exchange = ""
    if currency != Currency.PLN:
        exchange = str(get_rate(currency, Currency.PLN))
        print(f"Exchange {currency!s}-{Currency.PLN!s}: {exchange}")

    payment = PaymentField(
        transaction=TransactionType.FOREIGN,
        paymentType=PaymentType.TRANSFER,
        status=PaymentStatus.UNPAID,
        expiresAt=today + timedelta(days=7),
        exchange=exchange,
    )

    invoice = SalesInvoiceProformaForm(
        number=number,
        soldAt=today,
        mailOnStatusChange=True,
        sendToContractorOnAccept=True,
        contractor=contractor,
        contractorName=settings.CONTRACTOR_NAME,
        currency=currencies[currency_name],
        bankAccount=bank_accounts[bank_account],
        _token=token,
        services=services,
        payment=payment,
    )

    return invoice
