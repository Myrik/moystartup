from uuid import UUID

from requests import Session

from schemes import API


def get_contractor_id(session: Session, owner_id: UUID, name: str) -> UUID:
    contractors = session.get(
        f"{API}/contractors/offeror/{owner_id}", params={"name": ""}
    ).json()
    contractors = {i["name"]: UUID(i["id"]) for i in contractors}
    return contractors[name]
