import requests

from schemes import Currency


def get_rate(source: Currency, target: Currency) -> float:
    result = requests.get(
        "https://wise.com/rates/history+live",
        params={
            "source": source,
            "target": target,
            "length": 1,
            "resolution": "hourly",
        },
    ).json()[-1]
    return result["value"]
