#!/usr/bin/env python
import json
from json import JSONDecodeError
from time import sleep

import typer

from config import settings
from create_invoice import create_invoice
from get_session import get_session
from push_invoice import push_invoice
from schemes import (
    Currency,
    SalesInvoiceProformaForm,
)

app = typer.Typer()


def save_file_log(invoice: SalesInvoiceProformaForm):
    fdata = []
    try:
        with open("result.json", "r") as f:
            fdata = json.load(f)
    except (FileNotFoundError, JSONDecodeError):
        pass

    with open("result.json", "w") as f:
        fdata.append(json.loads(invoice.json()))
        json.dump(fdata, f, indent=True, ensure_ascii=False)


@app.command()
def create(currency: Currency):
    s = get_session(settings.USERNAME, settings.PASSWORD)
    overtime = int(input("Input owertime hours [0]: ") or 0)
    invoice = create_invoice(s, currency, overtime=overtime)

    print(invoice.json(indent=True))
    save_file_log(invoice)
    sleep(3)

    push_invoice(s, invoice)


@app.command()
def debug(currency: Currency, full: bool = False):
    s = get_session(settings.USERNAME, settings.PASSWORD)
    overtime = int(input("Input owertime hours [0]: ") or 0)
    invoice = create_invoice(s, currency, overtime=overtime)
    print(f"Gross: {sum(i.gross for i in invoice.services.values())} {currency}")
    if full:
        print(invoice.json(indent=True))


if __name__ == "__main__":
    app()
