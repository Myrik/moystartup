from requests import Session

from schemes import SITE, SalesInvoiceProformaForm


def push_invoice(session: Session, invoice: SalesInvoiceProformaForm):
    data = invoice.output()

    result = session.post(
        f"{SITE}/sales-invoices/add",
        data=invoice.output(),
    )

    if "alert alert-danger" in result.text:
        raise Exception(f"Error in form: {data}")
    if invoice.number not in result.text:
        raise Exception(f"No invoice on page: {result.url}")
