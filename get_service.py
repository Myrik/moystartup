from uuid import UUID

from requests import Session

from config import settings
from schemes import API, Service


def get_service(session: Session, owner_id: UUID) -> Service:
    service_id = session.get(
        f"{API}/warehouse/owner/{owner_id}/services",
        params={"name": settings.SERVICE_NAME},
    ).json()[0]["id"]
    service = session.get(f"{API}/warehouse/services/{service_id}").json()
    return Service(**service)
